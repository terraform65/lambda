provider "aws" {
  profile = "default"
  region  = "us-east-2"
}

data "archive_file" "code" {
  type        = "zip"
  source_file = "code/hello.py"
  output_path = "out/hello.zip"
}
data "aws_arn" "db_instance" {
  arn = "arn:aws:iam::311668130969:role/TLS-SPEKE-SPEKEServerLambdaRole-18SG8O9Y23OG3"
}
resource "aws_lambda_function" "test_lambda" {
  filename      = "out/hello.zip"
  function_name = "hello"
  role          = data.aws_arn.db_instance.arn
  handler       = "hello.lambda_handler"

  source_code_hash = filebase64sha256("out/hello.zip")

  runtime = "python3.8"
}
resource "aws_apigatewayv2_api" "api" {
  name          = "api-gateway"
  description   = "Proxy to handle requests to our API"
  protocol_type = "HTTP"
}
resource "aws_apigatewayv2_integration" "example" {
  api_id           = aws_apigatewayv2_api.api.id
  integration_type = "AWS_PROXY"

  connection_type           = "INTERNET"
  description               = "Lambda example"
  integration_method        = "POST"
  payload_format_version    = "2.0"
  integration_uri           = aws_lambda_function.test_lambda.invoke_arn
}
resource "aws_apigatewayv2_route" "resource" {
  api_id    = aws_apigatewayv2_api.api.id
  route_key = "GET /"
  target    = "integrations/${aws_apigatewayv2_integration.example.id}"
}
resource "aws_apigatewayv2_stage" "example" {
  api_id      = aws_apigatewayv2_api.api.id
  name        = "$default"
  auto_deploy = true
}
resource "aws_lambda_permission" "demo_users_get_function_allow_api_gateway" {
  depends_on = [
    aws_apigatewayv2_integration.example,
    aws_lambda_function.test_lambda
  ]
  function_name = aws_lambda_function.test_lambda.function_name
  statement_id = "AllowExecutionFromApiGateway_users_get"
  action = "lambda:InvokeFunction"
  principal = "apigateway.amazonaws.com"
  source_arn = "${aws_apigatewayv2_api.api.execution_arn}/*/*/*"
}